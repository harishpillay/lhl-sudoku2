## Note ##

The file was made available on https://www.facebook.com/leehsienloong/photos/a.344710778924968.83425.125845680811480/905828379479869/?type=1&theater
and on Google Docs: https://drive.google.com/folderview?id=0B2G2LjIu7WbdfjhaUmVzc1lCR2hUdk5fZllCOHdtbFItbU5qYzdqZGVxdmlnRkJyYVQ4VU0&usp=sharing&usp=sharing&urp=https://drive.google.com/folderview?id%3D0B2G2LjIu7W#list

* No license was stated. 
* PM Lee has been informed that he should probably add a license. Here
  is his reply:

<pre><code>

Message Classification: Restricted
   Thanks, Harish. Someone else pointed this out to me too, and had suggested to use the MIT Licence. I am working on this and will let you know, so you can add the licence to the code on you gitlab site too.
   
   -----Original Message-----
   From: Harish Pillay
   Sent: 6 May 2015 9:15 AM
   To: PMO Hsien Loong LEE (PMO)
   Subject: License to your sudoku source code
   
   PM Lee -
   
   Hi. First, thanks for posting the code to your sudoku solution. Your one act of showing off your coding skills has given significant validation that coding is both fun and enormously valuable.
   
   The code you posted did not have a license statement nor author statement which is important to have.
   
   In the meantime, I've taken the liberty of hosting it on gitlab.com[0] so that more people can collaborate and build on it.
   
   It would be good if you could provide a license that you want it to be placed under. There are many to choose from - opensource.org is a good location. I'd recommend that you consider using the GNU General Public License v3 (http://gplv3.fsf.org/) so that the code remains open forever.
   
   Of course, as primary author, you have full control of the code whether or not it is on a Free and Open Source license.
   
   Looking forward to hearing from you.
   
   [0] https://gitlab.com/harishpillay/lhl-sudoku2
   --
   Harish Pillay h.pillay@ieee.org gpg id: 746809E3
</pre></code>

* License.txt added. MIT license chosen.

